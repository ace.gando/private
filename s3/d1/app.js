// module
	// const lag = require("./functions");

	// console.log("This is from app.js");

	// lag.logger();

// CALLBACK functions
	// const fruits = ["banana","apple","saging"];
	// fruits.forEach(function(val){
	// 	console.log(val)
	// })	

	// or

	// const fruits = ["banana","apple","saging"];
	// function neyney(val){
	// 	console.log(val)
	// }
	// fruits.forEach(neyney)	

// DESTRUCTURING
	// const person = {name : "Alex", age: 16, mayInternet : true}
	// const { name, age, mayInternet} = person;
	// console.log(name)

	// const args = [1,2,3,4,5];
	// const [a, b, c, d, e] = args;
	// console.log(a, b);

// args
	// function greet(message = "Hello World!"){
	// 	console.log(message)
	// }

	// greet();

// SPREAD OPERATOR
	// let myArray = [1,2,3,4];

	// let newArray = [5, ...myArray, 7];

	// console.log(newArray);