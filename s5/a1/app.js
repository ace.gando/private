const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const app = express();
const port = 3000;

mongoose.connect(
	'mongodb://localhost:27017/message_board',
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
	}
);

mongoose.connect('connected', () => {
	console.log('Connected');
})

app.use(bodyParser.json());

app.use('/users', require('./routes/users'));
app.use('/messages', require('./routes/messages'));



app.use(function(err,req,res,next){
	res.status(400).json({
		error : err.message
	})
})

app.listen(port, () => {
	console.log(`Listening to port ${port}`);
})