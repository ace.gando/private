const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const MessageSchema = new Schema({
	title : {
		type : String,
		required : [true, "Title is required"]
	},
	description : {
		type : String,
		required : [true, "Description is required"]
	},
	likes : {
		type : Number,
		required : false
	},
	author : {
		type : String,
		required : [true, "Author is required"]
	}
});

module.exports = mongoose.model('Message', MessageSchema);