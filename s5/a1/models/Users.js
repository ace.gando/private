const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
	firstname : {
		type : String,
		required : [true, "First Name is required"]
	},
	lastname : {
		type : String,
		required : [true, "Last Name is required"]
	}
});

module.exports = mongoose.model('User', UserSchema);