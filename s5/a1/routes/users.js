const express = require('express');
const router = express.Router();
const UserModel = require('../models/Users')

// index
router.get('/', (req,res,next) => {
	UserModel.find()
	.then(users => {
		res.json(users)
	})
	.catch(next)
});

// single
router.get('/:id', (req,res,next) => {
	UserModel.findOne({_id : req.params.id})
	.then(user => res.json(user))
	.catch(next)
});

// create
router.post('/', (req,res,next) => {
	UserModel.create(
		{
			firstname : req.body.firstname,
			lastname : req.body.lastname
		}
	)
	.then((user) => res.json(user))
	.catch(next)
})

// update
router.put('/:id', (req,res,next) => {
	UserModel.findOneAndUpdate(
		{
			firstname : req.body.firstname,
			lastname : req.body.lastname
		}
	)
	.then(user => res.json(user))
	.catch(next)
})

// delete
router.delete('/:id', (req,res,next) => {
	UserModel.findOneAndDelete({_id : req.params.id})
	.then(user => res.json(user))
	.catch(next)
})

module.exports = router;