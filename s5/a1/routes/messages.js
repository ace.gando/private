const express = require('express');
const router = express.Router();
const MessageModel = require('../models/Messages')

// index
router.get('/', (req,res,next) => {
	MessageModel.find()
	.then(messages => {
		res.json(messages)
	})
	.catch(next)
})

// single
router.get('/:id', (req,res,next) => {
	MessageModel.findOne({_id : req.params.id})
	.then(message => res.json(message))
	.catch(next)
})

// create
router.post('/', (req,res,next) => {
	MessageModel.create(
	{
		title : req.body.title,
		description : req.body.description,
		likes : req.body.likes,
		author : req.body.author
	})
	.then(message => res.json(message))
	.catch(next)
})

// update
router.put('/:id', (req,res,next) => {
	MessageModel.findOneAndUpdate(
		{
			title : req.body.title,
			description : req.body.description,
			likes : req.body.likes,
			author : req.body.author		
		}
	)
	.then(message => res.json(message))
	.catch(next)
})

// delete
router.delete('/:id', (req,res,next) => {
	MessageModel.findOneAndDelete({_id : req.params.id})
	.then(message => res.json(message))
	.catch(next)
})

module.exports = router;